// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SiteTitle = 'Jetbov - Release Notes';
export const SiteDescription = 'Informações sobre novidades e atualizações do Jetbov';
